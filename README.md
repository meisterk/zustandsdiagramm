# zustandsdiagramm

Svelte-App als Übung zu UML-Zustandsdiagrammen.
Die Webapp soll ausprobiert und ein passendes Zustandsdiagramm gezeichnet werden.

Die Website befindet sich auf Codeberg-Pages: https://meisterk.codeberg.page/zustandsdiagramm/

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

## Deploy
- `npm run build`
- Inhalt des build-Ordners irgendwo anders hin kopieren
- `git checkout pages`
- alles ausser .git löschen
- Inhalt von build reinkopieren
- `git push`

